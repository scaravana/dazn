import React from 'react';

import ResultRow from "./ResultRow";

class ResultArea extends React.Component {

    render() {
        const rows = [];

        if(this.props.results) {
            this.props.results.forEach((result, i) => {
                let displayName = result.name || result.title || result.original_title;
                let date = (result.release_date) ? (result.release_date.substring(0,4)) : '';
                // let displayName = props.result.name || props.result.title || props.result.original_title;
                // console.log(JSON.stringify(result));

                if (displayName) {
                    rows.push(
                        <ResultRow key={i}
                                   type={result.media_type}
                                   date={date}
                                   displayName={displayName}
                                   overview={result.overview}
                        />
                    );
                }
            });
        }

        if (!rows.length) return (<div><h2>No results</h2></div>);
        else return (
            <div>
                <h2>
                    Results
                </h2>
                <table className="pure-table">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Year</th>
                        <th>Decription</th>
                    </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
            </div>
        );
    }
}

export default ResultArea