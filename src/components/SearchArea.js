import React from 'react';

class SearchArea extends React.Component {

    constructor(props) {
        super(props);
        this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
    }

    handleSearchTextChange(e) {
        this.props.onSearchTextChange(e.target.value);
    }

    render() {
        return (
            <form className="pure-form" >
                <input
                    type="text"
                    placeholder="Search movie..."
                    onChange={this.handleSearchTextChange}
                />
            </form>
        );
    }
}

export default SearchArea;