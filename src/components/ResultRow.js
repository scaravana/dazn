import React from 'react';

const  ResultRow = (props) => {

    return (
            <tr>
                <td>{props.type}</td>
                <td>{props.displayName}</td>
                <td>{props.date}</td>
                <td>{props.overview}</td>
            </tr>
    )
};

export default ResultRow