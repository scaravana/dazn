import React from 'react';
import renderer from 'react-test-renderer';
import ReactDOM from 'react-dom';
import toJson from 'enzyme-to-json';
import { shallow } from 'enzyme';

import SearchArea from "./SearchArea";
import App from "../App";

test('render search bar', () => {


    const tree = renderer.create(
        <searchArea />
    );
    const component = shallow(<searchArea/>);

    component.simulate('change', { target: {
            value: 'batman' }
    });

    expect(toJson(component)).toMatchSnapshot();

});
