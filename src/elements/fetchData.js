function getMovieData(searchText) {
    return new Promise(function (resolve, reject) {
        const api_key = "3763560b013c9c9889c148d2fac176e9";
        const base_uri = "http://api.themoviedb.org/3/";
        const images_uri = "http://image.tmdb.org/t/p/";
        const timeout = 5000;
        const language = "en-US";
        let myOptions, query, option;

        myOptions = "search/multi?query=" + searchText;
        query = "&api_key=" + api_key + "&language=" + language;


        fetch(base_uri + myOptions + query).then(function (response) {
            return response.json();
        }).then(function (jsonResponse) {
            resolve(jsonResponse);
        }).catch(function (error) {
            reject(error);
        });
    });
};

export { getMovieData };
