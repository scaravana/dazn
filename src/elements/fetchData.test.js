import { getMovieData } from "./fetchData";
import * as data from '../../tests/data/fetchResponse';

describe('testing api', () => {
    beforeEach(() => {
        fetch.resetMocks()
    });

    it('calls movieDB search', () => {

        fetch.mockResponse(JSON.stringify(data))
        getMovieData("batman")

        expect(fetch.mock.calls.length).toEqual(1)
        expect(fetch.mock.calls[0][0]).toEqual('http://api.themoviedb.org/3/search/multi?query=batman&api_key=3763560b013c9c9889c148d2fac176e9&language=en-US')
    })
});