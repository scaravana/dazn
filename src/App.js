import React, {Component} from 'react';
import SearchArea from "./components/SearchArea";
import ResultArea from "./components/ResultArea";
import { getMovieData } from "./elements/fetchData";



class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            results:[]
        };

        this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
    }

    handleSearchTextChange(searchText) {
        this.setState({
            searchText: searchText
        });
        getMovieData(searchText).then(results => {
            this.setState({
                results: results.results
            });
        }).catch(e => {
            console.log(e);
        })
    }

    render() {
        return (
            <div className="App" className="pure-g" >
                <header id="topArea" className="pure-u-1-1">
                    <h1 id="title">Movie search</h1>
                </header>
                <div id="bodyArea" className="pure-g">
                    <div id="searchArea"  className="pure-u-1">
                        <SearchArea
                            searchText={this.state.searchText}
                            onSearchTextChange={this.handleSearchTextChange}
                        />
                    </div>
                    <div id="resultArea"  className="pure-u-1">
                        <ResultArea
                            results={this.state.results}
                            searchText={this.state.searchText}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
