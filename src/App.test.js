import React from 'react';
import App from './App';
import renderer from 'react-test-renderer';

test('Load data', () => {
    const component = renderer.create(
        <App></App>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

});
